## Steps to setup
mvn clean install
java -jar target/websearcher-1.0-SNAPSHOT.jar -Djava.net.preferIPv4Stack=true


## Problem Statement

#Website Searcher
Given a list of urls in urls.txt: https://s3.amazonaws.com/fieldlens-public/urls.txt, write a program that will fetch each page and determine whether a search term exists on the page (this search can be a really rudimentary regex - this part isn't too important).

You can make up the search terms. Ignore the addition information in the urls.txt file.

Constraints
    Search is case insensitive
    Should be concurrent.
    But! It shouldn't have more than 20 HTTP requests at any given time.
    The results should be writted out to a file results.txt
    Avoid using thread pooling libraries like Executor, ThreadPoolExecutor, Celluloid, or Parallel streams.
    The solution must be written in Kotlin or Java.

Sample urls.txt: https://s3.amazonaws.com/fieldlens-public/urls.txt

The solution must be able to be run from the command line (dont assume JDK is available):
java -jar ./jarFileName.jar

# Question
1. What is the primary purpose of this test? Is it mainly focused on candidate's approach to introducing concurrency without thread pooling libraries or how effectively he/she scans a search term or both?

2. Is it ok to use java.util.concurrent.locks/java.util.concurrent.Semaphore in this solution?

#Answer
The primary purpose of the test is to assess your understanding of multi-threading. We have not explicitly prohibited java.util.concurrent.Semaphore, so it should be fine.
