package com.wework.validator;

import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Service;

/**
 * The type Input file validator.
 */
@Service
public class InputFileValidator {

    /**
     * Validate header line.
     *
     * @param header the header
     * @return the boolean
     */
    public boolean validateHeader(String header) {
        if (header == null || header.isEmpty()) return false;
        String[] headers = header.split(",");
        if (headers.length != 6) return false;
        // ToDo more validations
        return true;
    }

    /**
     * Validate site url.
     *
     * @param url the url
     * @return the boolean
     */
    public boolean validateSiteUrl(String url) {
        if (url == null || url.isEmpty()) return false;
        String[] headers = url.split(",");
        if (headers.length != 6) return false;
        if (!StringUtil.isNumeric(headers[0])) return false;   // Rank
        if (headers[1] == null || headers[1].isEmpty()) return false; // URL
        // ToDo more validations
        return true;
    }

}
