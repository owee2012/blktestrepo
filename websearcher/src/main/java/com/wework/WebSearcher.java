package com.wework;

import com.wework.model.SiteUrl;
import com.wework.service.FileService;
import com.wework.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.exit;

/**
 * The type Web searcher.
 */
@SpringBootApplication
@Profile("!test")
public class WebSearcher implements CommandLineRunner {

    /**
     * The Index service.
     */
    @Autowired
    private IndexService indexService;

    /**
     * The File service.
     */
    @Autowired
    private FileService fileService;

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     *
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {

        SpringApplication app = new SpringApplication(WebSearcher.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);

    }

    /**
     * Executes the main program.
     * Three main objectives
     * 1. Load the input url text file.
     * 2. Download the Content and Build Search Index (One time process)
     * 3. Start accepting search term from the user and save the results to output file.
     *
     * @param args the args
     *
     * @throws Exception the exception
     */
    @Override
    public void run(String... args) throws Exception {
        System.out.println("Hi! Welcome to Web Searcher.");
        String inputFile = "urls.txt";
        String outputFile = "results.txt";

        for (int i = 0; i < args.length; i++) {
            if (args[i].equalsIgnoreCase("-i") && i + 1 < args.length) {
                inputFile = args[i + 1];
            } else if (args[i].equalsIgnoreCase("-o") && i + 1 < args.length) {
                outputFile = args[i + 1];
            }
        }

        System.out.println("All Good. Let me first build the index from the given URL file - " + inputFile);

        indexService.buildSearchIndex(inputFile);

        System.out.println("Search Indexes built.");
        System.out.println("--------------------------------------");
        System.out.println("--------------------------------------");
        System.out.println("--------------------------------------");
        System.out.println("Enter the Search Term - ");

        InputStream source = System.in;
        Scanner in = new Scanner(source);
        while (in.hasNext()) {
            String[] terms = in.nextLine().split("\\s");
            if (terms != null && terms.length > 0) {
                List<SiteUrl> results = indexService.search(terms[0]);
                if (results != null) {
                    System.out.println("Search Term found on " + results.size() + " pages.");
                    fileService.updateResults(outputFile, terms[0], results);
                    System.out.println("Detailed result has been dumped to " + outputFile);
                }
                System.out.println("Try with new search term!");
            }
        }
        exit(0);
    }
}
