package com.wework.util;

import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Page downloader.
 */
@Component
public class PageDownloader {
    /**
     * The Max connections.
     */
    @Value("${maxConnections}")
    private int maxConnections;

    /**
     * The Timeout.
     */
    @Value("${httpTimeout}")
    private int timeout;

    /**
     * The User agent.
     */
    final String userAgent = "Mozilla/5.0";
    /**
     * The constant className.
     */
    final static String className = PageDownloader.class.getName();


    /**
     * Load site content.
     *
     * @param urls the urls
     * @return the map
     * @throws InterruptedException the interrupted exception
     */
    public Map<String, String> loadSiteContent(final List<String> urls) throws InterruptedException {
        if (urls == null || urls.isEmpty()) return null;

        Map<String, String> contentMap = new ConcurrentHashMap();
        final Semaphore semaphore = new Semaphore(maxConnections);

        for (String url : urls) {
            try {
                semaphore.acquire();
                Runnable task = () -> {
                    String location = url;
                    if (!location.toLowerCase().matches("^\\w+://.*")) {
                        location = "http://" + location;
                    }
                    try {
                        String content = Jsoup.connect(location)
                                .userAgent(userAgent).timeout(timeout)
                                .get().html();
                        contentMap.put(url, content);
                        Logger.getLogger(className).log(Level.INFO, "Page Loaded " + location);
                    } catch (IOException ex) {
                        Logger.getLogger(className).log(Level.SEVERE, ex.getMessage() + " " + location);
                    } finally {
                        semaphore.release();
                    }
                };
                new Thread(task).start();

            } catch (InterruptedException ex) {
                semaphore.release();
            }
        }

        while (semaphore.availablePermits() < maxConnections) { //if still have running child thread, wait for a sec.
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Logger.getLogger(className).log(Level.SEVERE, "Main Thread Interrupted.", e);
                throw e;
            }
        }
        return contentMap;
    }
}
