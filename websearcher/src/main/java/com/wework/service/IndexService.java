package com.wework.service;

import com.wework.model.SiteUrl;
import com.wework.util.PageDownloader;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * The type Index service.
 */
@Service
public class IndexService {

    /**
     * The Page downloader.
     */
    @Autowired
    private PageDownloader pageDownloader;

    /**
     * The File service.
     */
    @Autowired
    private FileService fileService;

    /**
     * The Site url list.
     */
    private List<SiteUrl> siteUrlList;
    /**
     * The Index map.
     */
    private Map<String, List<SiteUrl>> indexMap;
    /**
     * The Class name.
     */
    private final String className = IndexService.class.getName();

    /**
     * Build search index.
     * Three step process
     * 1. Load the file
     * 2. Load the web page content
     * 3. Build search Index
     *
     * @param inputFile the input file
     *
     * @throws Exception the exception
     */
    public void buildSearchIndex(String inputFile) throws Exception {
        siteUrlList = fileService.loadUrlFile(inputFile);

        if (siteUrlList == null && siteUrlList.isEmpty()) throw new Exception("Empty input URL file");

        List<String> urls = siteUrlList.stream().map(SiteUrl::getUrl).collect(Collectors.toList());
        Map<String, String> urlContentMap = pageDownloader.loadSiteContent(urls);
        siteUrlList.forEach(x -> x.setContent(urlContentMap.get(x.getUrl())));

        indexMap = buildIndex(siteUrlList);
    }

    /**
     * Build index. Uses JSoup library to extract text from HTML String content and build Search Index
     * data structure.
     * Map<String searchTerm, List<SiteUrl>results>
     *
     * @param siteUrlList the site url list
     *
     * @return the map
     */
    protected Map<String, List<SiteUrl>> buildIndex(final List<SiteUrl> siteUrlList) {
        Map<String, List<SiteUrl>> searchTermMap = new LinkedHashMap();
        for (SiteUrl siteUrl : siteUrlList) {
            String content = siteUrl.getContent();
            if (content != null && !content.isEmpty()) {
                String lowerCaseText = Jsoup.parse(content).text().toLowerCase();
                String[] unique = Arrays.stream(lowerCaseText.split("\\s")).distinct().toArray(String[]::new);
                for (String searchTerm : unique) {
                    searchTermMap.compute(searchTerm, (key, value) -> {
                        if (value == null) {
                            value = new LinkedList();
                        }
                        value.add(siteUrl);
                        return value;
                    });
                }
            }
        }
        return searchTermMap;
    }


    /**
     * Search the index for given search term.
     *
     * @param searchTerm the search term
     *
     * @return the list
     */
    public List<SiteUrl> search(String searchTerm) throws Exception {
        if (indexMap == null || indexMap.isEmpty()) {
            Logger.getLogger(className).log(Level.SEVERE, "Web Pages has not been loaded yet. Check the input URL text file!");
            throw new Exception("Error in input url text file");
        }

        if (searchTerm == null || searchTerm.isEmpty()) return null;
        String search = searchTerm.toLowerCase();
        if (indexMap.containsKey(search)) return indexMap.get(search);

        // Incase no search term found on already built index, fall back to rudimentary regex search.
        return siteUrlList.stream().filter(url ->
                url.getContent() != null && Pattern.compile(search).matcher(url.getContent()).find()).
                collect(Collectors.toList());
    }

}
