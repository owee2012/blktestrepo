package com.wework.model;

import java.math.BigInteger;

/**
 * The Site url Model.
 */
public class SiteUrl {
    /**
     * The Rank.
     */
    int rank;
    /**
     * The Url.
     */
    String url;
    /**
     * The Content.
     */
    String content;
    /**
     * The Link root domains.
     */
    BigInteger linkRootDomains;
    /**
     * The External links.
     */
    BigInteger externalLinks;
    /**
     * The Moz rank.
     */
    double mozRank;
    /**
     * The Moz trust.
     */
    double mozTrust;

    /**
     * Instantiates a new Site url.
     *
     * @param rank the rank
     * @param url  the url
     */
    public SiteUrl(int rank, String url) {
        this.rank = rank;
        this.url = url;
    }

    /**
     * Gets rank.
     *
     * @return the rank
     */
    public int getRank() {
        return rank;
    }

    /**
     * Sets rank.
     *
     * @param rank the rank
     */
    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets url.
     *
     * @param url the url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Gets link root domains.
     *
     * @return the link root domains
     */
    public BigInteger getLinkRootDomains() {
        return linkRootDomains;
    }

    /**
     * Sets link root domains.
     *
     * @param linkRootDomains the link root domains
     */
    public void setLinkRootDomains(BigInteger linkRootDomains) {
        this.linkRootDomains = linkRootDomains;
    }

    /**
     * Gets external links.
     *
     * @return the external links
     */
    public BigInteger getExternalLinks() {
        return externalLinks;
    }

    /**
     * Sets external links.
     *
     * @param externalLinks the external links
     */
    public void setExternalLinks(BigInteger externalLinks) {
        this.externalLinks = externalLinks;
    }

    /**
     * Gets moz rank.
     *
     * @return the moz rank
     */
    public double getMozRank() {
        return mozRank;
    }

    /**
     * Sets moz rank.
     *
     * @param mozRank the moz rank
     */
    public void setMozRank(double mozRank) {
        this.mozRank = mozRank;
    }

    /**
     * Gets moz trust.
     *
     * @return the moz trust
     */
    public double getMozTrust() {
        return mozTrust;
    }

    /**
     * Sets moz trust.
     *
     * @param mozTrust the moz trust
     */
    public void setMozTrust(double mozTrust) {
        this.mozTrust = mozTrust;
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets content.
     *
     * @param content the content
     */
    public void setContent(String content) {
        this.content = content;
    }

}
